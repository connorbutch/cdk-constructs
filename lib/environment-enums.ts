export enum StageName {
  DEV = 'dev',
  BETA = 'beta',
  GAMMA = 'gamma',
  PROD = 'prod'
}