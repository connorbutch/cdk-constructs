import * as cdk from 'aws-cdk-lib';
import {App, Stack} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {FunctionsWithRollback} from "./function-with-rollback";
import {Function, InlineCode, Runtime} from "aws-cdk-lib/aws-lambda";
import {StageName} from "./environment-enums";

export class CdkConstructsStack extends Stack {
  private static readonly EXAMPLE_FUNCTION_NAME = "exampleFunction"

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const app = new App();

    // if we want to throw an exception to test the alarms/rollback,
    // uncomment the below lines starting with #, and invoke the
    // lambda passing an odd number as the "numberOfItems" fields in json payload
    /*
    {
       "numberOfItems": 1
    }
     */
    const pythonLambdaCode: string = `
import json

def lambda_handler(event, context):
#    if "numberOfItems" in event:
#        number_of_items = event["numberOfItems"]
#        if number_of_items % 2 != 0:
#            raise Exception("Oh no, our code doesn't handle an odd number of items: " + str(number_of_items))

    return {
        'statusCode': 200,
        'body': json.dumps('Hello, world! This is an AWS Lambda function. Different version')
    }
    `;

    const exampleFunction = new Function(this, "ExampleFunction", {
      code: new InlineCode(pythonLambdaCode),
      handler: "index.lambda_handler",
      runtime: Runtime.PYTHON_3_8,
      functionName: CdkConstructsStack.EXAMPLE_FUNCTION_NAME
    })
    const functionsWithRollback = new FunctionsWithRollback(this, "FunctionsWithRollback", {
      stageName: StageName.DEV,
      metricNamespace: "exampleNamespace",
      applicationName: "exampleApplication",
      lambdas: [
          exampleFunction
      ]
    });
    app.synth();
  }
}
