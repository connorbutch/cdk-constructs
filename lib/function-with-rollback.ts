import {Construct} from "constructs";
import {
    Alarm,
    AlarmRule,
    ComparisonOperator,
    CompositeAlarm,
    MathExpression,
    Metric,
    TreatMissingData
} from "aws-cdk-lib/aws-cloudwatch";
import {LogGroup, MetricFilter} from "aws-cdk-lib/aws-logs";
import {Duration} from "aws-cdk-lib";
import {Alias, Function} from "aws-cdk-lib/aws-lambda";
import {LambdaDeploymentConfig, LambdaDeploymentGroup} from "aws-cdk-lib/aws-codedeploy";
import {StageName} from "./environment-enums";

interface FunctionsWithRollbackProps {
    readonly metricNamespace: string,
    readonly applicationName: string,
    readonly lambdas: Function[],
    readonly stageName: StageName,
    //this should take in concept of environment (beta, gamma, prod, etc) so that it can
    //apply longer rolling deploy in prod, medium length and gamma
    // and likely not at all in beta
    readonly additionalAlarmsToRollbackOn?: Alarm[]
}

export class FunctionsWithRollback extends Construct {

    private static readonly STAGE_TO_DEPLOYMENT_MAP = new Map([
        [StageName.DEV, LambdaDeploymentConfig.ALL_AT_ONCE],
        [StageName.BETA, LambdaDeploymentConfig.ALL_AT_ONCE],
        [StageName.GAMMA, LambdaDeploymentConfig.LINEAR_10PERCENT_EVERY_3MINUTES],
        [StageName.PROD, LambdaDeploymentConfig.LINEAR_10PERCENT_EVERY_10MINUTES]
    ])

    private static readonly TIMEOUT_LOG_STRING: string = "Task timed out after";
    private static readonly LAMBDA_METRIC_NAMESPACE: string = "AWS/Lambda";

    constructor(scope: Construct, id: string, props: FunctionsWithRollbackProps) {
        super(scope, id);

        const alarms: Alarm[] = []
        if (props.additionalAlarmsToRollbackOn && props.additionalAlarmsToRollbackOn.length > 0) {
            alarms.push(...props.additionalAlarmsToRollbackOn)
        }

        const aliases: Alias[] = props.lambdas.map(lambda => {
            //build alarms for specific version -- this will catch errors sooner
            //consider new version only gets 10% of traffic, but is erroring 40% of time
            //overall function error rate will be 4%, which might not trip our alarm
            //however, the version error rate will be 40%, which will certainly trip alarm
            const versionSuccessRateAlarm = this.buildFunctionVersionSuccessRateAlarm(lambda)
            alarms.push(versionSuccessRateAlarm)

            const versionThrottleRateAlarm = this.buildFunctionVersionThrottleRateAlarm(lambda)
            alarms.push(versionThrottleRateAlarm)

            const timeoutAlarm = this.buildFunctionTimeoutAlarm(lambda, props)
            alarms.push(timeoutAlarm)

            const overallFunctionSuccessAlarm = this.buildFunctionOverallSuccessRateAlarm(lambda)
            alarms.push(overallFunctionSuccessAlarm)

            return new Alias(this, `${lambda.node.id}LiveAlias`, {
                aliasName: "live",
                version: lambda.currentVersion
            })
        })

        const applicationCompositeAlarm = new CompositeAlarm(this, `${props.applicationName}CompositeAlarm`, {
            alarmRule: AlarmRule.anyOf(...alarms)
        })

        //vary the rollout strategy -- for beta/personal dev account we just want to rollout all at once so we can test new code
        //for gamma, do rolling deploy, but less bake time than prod
        //roll out slowest in prod
        const deploymentConfig = FunctionsWithRollback.STAGE_TO_DEPLOYMENT_MAP.get(props.stageName);

        aliases.forEach(alias => {
            const deploymentGroup = new LambdaDeploymentGroup(this, `${alias.node.id}DeploymentGroup`, {
                alias,
                deploymentConfig: deploymentConfig,
                alarms: [applicationCompositeAlarm]
                //in future, could add a post deploy hook that ensures the function has handled at least a certain number of requests
                //before continuing the deployment
                //this can be done by using canary traffic (i.e. cw synthetics) so even if there is no client traffic, you still reach
                //the required minimum
            })
        })
    }

    private buildFunctionTimeoutAlarm(lambdaFunction: Function, props: FunctionsWithRollbackProps) {
        const functionLogGroup = new LogGroup(this, `${lambdaFunction.node.id}LogGroup`, {
            logGroupName: `/aws/lambda/${lambdaFunction.functionName}`
        });
        const timeoutMetricFilter = new MetricFilter(this, `${lambdaFunction.node.id}TimeoutMetricFilter`, {
            filterPattern: {
                logPatternString: FunctionsWithRollback.TIMEOUT_LOG_STRING
            },
            logGroup: functionLogGroup,
            metricName: `${lambdaFunction.functionName}Timeout`,
            metricNamespace: props.metricNamespace,
            metricValue: "1"
        });
        return new Alarm(this, `${lambdaFunction.node.id}TimeoutAlarm`, {
            threshold: 0,
            comparisonOperator: ComparisonOperator.GREATER_THAN_THRESHOLD,
            evaluationPeriods: 10,
            datapointsToAlarm: 2,
            metric: timeoutMetricFilter.metric(),
            treatMissingData: TreatMissingData.NOT_BREACHING,
            alarmDescription: `The ${lambdaFunction.functionName} lambda function is timing out`
        });
    }

    private buildFunctionVersionSuccessRateAlarm(lambdaFunction: Function) {
        const newVersionErrorMetric = new Metric({
            metricName: "Errors",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
                ExecutedVersion: lambdaFunction.currentVersion.toString()
            }
        })
        const newVersionInvocationMetric = new Metric({
            metricName: "Invocations",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
                ExecutedVersion: lambdaFunction.currentVersion.toString()
            }
        })
        const newFunctionVersionSuccessRateMetric = new MathExpression({
            label: `${lambdaFunction.functionName}NewVersionSuccessRate`,
            expression: '100 - 100 * errors/invocations',
            period: Duration.minutes(1),
            usingMetrics: {
                errors: newVersionErrorMetric,
                invocations: newVersionInvocationMetric,
            },
        });
        return new Alarm(this, `${lambdaFunction.node.id}NewVersionSuccessRateAlarm`, {
            threshold: 99,
            comparisonOperator: ComparisonOperator.LESS_THAN_THRESHOLD,
            evaluationPeriods: 10,
            datapointsToAlarm: 2,
            metric: newFunctionVersionSuccessRateMetric,
            treatMissingData: TreatMissingData.NOT_BREACHING,
            alarmDescription: `The newest version of the ${lambdaFunction.functionName} lambda is encountering errors`
        });
    }

    private buildFunctionVersionThrottleRateAlarm(lambdaFunction: Function) {
        const newVersionThrottleCount = new Metric({
            metricName: "Throttles",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
                ExecutedVersion: lambdaFunction.currentVersion.toString()
            }
        })
        const newVersionInvocationMetric = new Metric({
            metricName: "Invocations",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
                ExecutedVersion: lambdaFunction.currentVersion.toString()
            }
        })
        const newFunctionVersionSuccessRateMetric = new MathExpression({
            label: `${lambdaFunction.functionName}NewVersionSuccessRate`,
            expression: '100 - 100 * throttles/invocations',
            period: Duration.minutes(1),
            usingMetrics: {
                throttles: newVersionThrottleCount,
                invocations: newVersionInvocationMetric,
            },
        });
        return new Alarm(this, `${lambdaFunction.node.id}NewVersionThrottleRateAlarm`, {
            threshold: 99,
            comparisonOperator: ComparisonOperator.LESS_THAN_THRESHOLD,
            evaluationPeriods: 10,
            datapointsToAlarm: 2,
            metric: newFunctionVersionSuccessRateMetric,
            treatMissingData: TreatMissingData.NOT_BREACHING,
            alarmDescription: `The newest version of the ${lambdaFunction.functionName} lambda is encountering errors`
        });
    }

    private buildFunctionOverallSuccessRateAlarm(lambdaFunction: Function) {
        const newVersionErrorMetric = new Metric({
            metricName: "Errors",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
            }
        })
        const newVersionInvocationMetric = new Metric({
            metricName: "Invocations",
            namespace: FunctionsWithRollback.LAMBDA_METRIC_NAMESPACE,
            dimensionsMap: {
                FunctionName: lambdaFunction.functionName,
            }
        })
        const newFunctionVersionSuccessRateMetric = new MathExpression({
            label: `${lambdaFunction.functionName}OverallSuccessRate`,
            expression: '100 - 100 * errors/invocations',
            period: Duration.minutes(1),
            usingMetrics: {
                errors: newVersionErrorMetric,
                invocations: newVersionInvocationMetric,
            },
        });
        return new Alarm(this, `${lambdaFunction.node.id}OverallSuccessRateAlarm`, {
            threshold: 99,
            comparisonOperator: ComparisonOperator.LESS_THAN_THRESHOLD,
            evaluationPeriods: 10,
            datapointsToAlarm: 2,
            metric: newFunctionVersionSuccessRateMetric,
            treatMissingData: TreatMissingData.NOT_BREACHING,
            alarmDescription: `The ${lambdaFunction.functionName} lambda is encountering errors`
        });
    }
}